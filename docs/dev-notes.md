# Project Initialization
## Project creation
- `$ nest new CoWorkAtHome_Back`
    - chose npm as package manager
- Save Nest & Git READMEs and create new blank README.md


## Add .env management
- `npm i --save @nestjs/config cross-env joi`
- follow https://dev.to/pitops/managing-multiple-environments-in-nestjs-71l


## Enable CORS
https://docs.nestjs.com/security/cors
- in main.ts, add: app.enableCors();


## Add ORM (Mongoose) + Add DTO Validators
https://docs.nestjs.com/techniques/mongodb
- `npm i --save @nestjs/mongoose mongoose`
- create MongooseService
- inject in AppModule
!! Doesn't work with localhost, use 127.0.0.1 instead
o Connect to MongoAtlas

- `npm i --save class-validator class-transformer`
- in main.ts, add: app.useGlobalPipes(new ValidationPipe());


## Add Swagger
- `npm install --save @nestjs/swagger`
- follow https://docs.nestjs.com/openapi/introduction


## Add Compodoc
https://wanago.io/2021/10/18/api-nestjs-documentation-compodoc-jsdoc/
o `npm install @compodoc/compodoc`
