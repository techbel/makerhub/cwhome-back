import { Inject, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { MongooseOptionsFactory } from '@nestjs/mongoose';
import { MongooseModuleOptions } from '@nestjs/mongoose/dist';

@Injectable()
export class MongooseService implements MongooseOptionsFactory {
    @Inject(ConfigService)
    private readonly _configService: ConfigService
    
    createMongooseOptions(): MongooseModuleOptions | Promise<MongooseModuleOptions> {
        return {
            uri: this._configService.get<string>('databaseUrl')
        };
    }
}
